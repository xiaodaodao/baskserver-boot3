# baskserver-boot3

# 简介
该项目是baskserver的SpringBoot的示例项目，其中包含了如下的功能：
* 管理控制台，用于管理用户、群组
* 报表的设计、开发和发布
* 数据流程的设计、开发和发布
* 仪表盘的设计、开发和发布
* 统一数据源的管理
* 计划任务的管理
* 文件资源的权限管理
* 操作日志的管理

# 配置说明
下载好项目后，首先要在src/main/resources目录下找到baskserver-init.properties配置文件设置其中的baskserver.home目录，例如设置为:
````properties
baskserver.home=c:/baskserver_home
````
> 要确保该目录存在。

之后就可以启动com.basksoft.server.demo.BaskServerApplication类。
正常情况下启动日志如下：
````properties
2023-07-11 22:34:25.910  INFO 16586 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2023-07-11 22:34:25.910  INFO 16586 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.58]
2023-07-11 22:34:26.009  INFO 16586 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2023-07-11 22:34:26.009  INFO 16586 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 1172 ms
2023-07-11 22:34:26.328  INFO 16586 --- [           main] c.b.c.config.bootstrap.BootstrapManager  : [BASKSERVER-CONSOLE]初始化...
2023-07-11 22:34:26.335  INFO 16586 --- [           main] c.b.c.config.bootstrap.BootstrapManager  : BaskServer Home:c:/baskserver_home
2023-07-11 22:34:26.341  WARN 16586 --- [           main] c.b.c.config.bootstrap.BootstrapManager  : 未能完成配置文件加载,需要先进行配置文件初始化工作.
2023-07-11 22:34:26.979  INFO 16586 --- [           main] o.s.b.a.e.web.EndpointLinksResolver      : Exposing 1 endpoint(s) beneath base path '/actuator'
2023-07-11 22:34:27.053  INFO 16586 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2023-07-11 22:34:27.073  INFO 16586 --- [           main] c.b.server.demo.BaskServerApplication    : Started BaskServerApplication in 2.869 seconds (JVM running for 3.458)
````
通过该日志可以看到BaskServer Home的路径配置，进一步确认配置是否正确。

# 系统初始化
现在就可以访问如下URL执行安装向导
> http://localhost:8080/baskserver

[安装向导详细使用说明](https://basksoft.com/doc/server/setup/setup_install.html)

向导初始化完成后，就可以通过如下url访问系统:
> http://localhost:8080/baskserver/login

默认初始化的用户为:admin，密码为:password

baskserver支持多租户场景，所以登录后，我们还需要创建一个团队，今后测试开发的资源都存储在该团队下。

现在就开始你的baskserver的试用吧。

详细产品使用文档:
> BaskServer:  https://basksoft.com/doc/server/

> BaskAdapt: https://basksoft.com/doc/adapt/

在线DEMO:
> DEMO https://demo.basksoft.com/baskserver

> 默认初始化的用户为:admin，密码为:password

# 项目文件说明
* ToolSpring.java是获取spring环境变量的工具类，其实现仅供参考
* BaskOriginFilter.java是为了支持服务器客户端分离场景下跨越问题，也可以直接用BaskFilter
* BaskServerConfig.java是为了注册BaskFilter控件，对于有扩展需求的可以在该处修改调整为BaskFilter的扩展类，例如本例的:BaskOriginFilter
* BaskConfigLoader.java类是考虑到某些部署场景下，系统架构师不想配置baskserver.home，而是希望用SpringBoot的环境变量配置baskserver所需的系统数据库连接参数的解决办法，该类要通过SPI机制在resources/META-INFO/services下添加com.basksoft.core.config.bootstrap.ApplicationConfigLoader文件配置，详细说明参考[线上文档](https://basksoft.com/doc/server/setup/setup_configloader.html)
* SampleCenterDatasource.java是定义baskserver业务数据源的一种参考实现，也是SPI的实现方式，在resources/META-INF/services下的com.basksoft.core.model.datasource.BuildinDatasourceProvider文件中做了类配置，这样我们就可以通过一个类提供统一的业务数据访问的数据源
* TestConnectionProvider.java是配合baskserver初始化使用的一个类，详细说明参考[线上文档](https://basksoft.com/doc/server/setup/setup_configloader.html)
* MockSecurityProvider.java 是用与免密登录，用SPI方式配置，在resources/META-INF/services下的com.basksoft.core.security.provider.SecurityProvider文件中做配置，内容设置为com.basksoft.server.demo.config.MockSecurityProvider,并且同时要修改数据库BASK_PROPERTY中KEY_为bask.security.enable的行的VALUE_值为false,并重新启动系统
* main/java/com/basksoft/demo/dataset目录下是一些自定义dataset的参考，主要用于报表和仪表盘中


# 联系信息

> QQ技术交流群 742169613

# 官网 

> 上海宝诗信息技术信息有限公司 https://basksoft.com/

# 常见错误
如果baskserver.home目录未配置或配置不正确，会出现如下启动错误：
````txt
Caused by: com.basksoft.core.config.exception.ConfigLoadException: 当前系统环境下未发现BASKSERVER_HOME相关的配置,请确认!
	at com.basksoft.core.config.HomeLocator.<clinit>(HomeLocator.java:44) ~[classes/:na]
	... 42 common frames omitted
````



