package com.basksoft.baskserver.demo;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class ToolSpring implements ApplicationContextAware {
	private static ApplicationContext applicationContext = null;
	private static Environment env;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		if (ToolSpring.applicationContext==null) {
			ToolSpring.applicationContext = applicationContext;
		}
	}

	public static Environment getBean(Class<Environment> clazz) {
		return ToolSpring.applicationContext.getBean(clazz);
	}
	
	public static Environment getEnvironment() {
		return env;
	}
	public static void setEnvironment(Environment environment) {
		env = environment;
	}
}
