package com.basksoft.baskserver.demo.config;

import com.basksoft.baskserver.demo.ToolSpring;
import com.basksoft.core.config.ApplicationConfig;
import com.basksoft.core.config.ConfigType;
import com.basksoft.core.config.bootstrap.ApplicationConfigLoader;

public class BaskConfigLoader implements ApplicationConfigLoader {

    @Override
    public ApplicationConfig load() {
        ApplicationConfig config = new ApplicationConfig();
        //可以读取自定义配置，例如spring相关配置信息
        config.getProperties().setProperty("baskserver.store.database.platform", "mysql");
        //ConfigType.jdbc
        config.setConfigType(ConfigType.jdbc);
        config.getProperties().setProperty("baskserver.store.database.username", getProperty("baskserver.sampledb.username"));
        config.getProperties().setProperty("baskserver.store.database.password", getProperty("baskserver.sampledb.password"));
        config.getProperties().setProperty("baskserver.store.database.driver", getProperty("baskserver.sampledb.driverClassName"));
        config.getProperties().setProperty("baskserver.store.database.url", getProperty("baskserver.sampledb.url"));
        config.getProperties().setProperty("baskserver.store.database.initialsize", "0");
        config.getProperties().setProperty("baskserver.store.database.maxTotal", "50");
        config.getProperties().setProperty("baskserver.store.database.minIdle", "2");
        config.getProperties().setProperty("baskserver.store.database.validationQuery", "select 1");
        config.getProperties().setProperty("baskserver.store.database.maxIdle", "10");
        
        //ConfigType.jndi
//        config.setConfigType(ConfigType.jndi);
//        config.getProperties().setProperty("baskreport.store.database.jndiname", "java:comp/env/sample");
   
        //ConfigType.connection
//        config.setConfigType(ConfigType.connection);
//        config.getProperties().setProperty("baskreport.store.database.classname", "com.basksoft.baskserver.demo.service.TestConnectionProvider");
        
        return config;
    }
    private static String getProperty(String key) {
        return ToolSpring.getEnvironment().getProperty(key);
    }
}
