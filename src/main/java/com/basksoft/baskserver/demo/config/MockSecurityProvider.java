package com.basksoft.baskserver.demo.config;

import java.util.ArrayList;

import jakarta.servlet.http.HttpServletRequest;

import com.basksoft.core.database.model.Tenant;
import com.basksoft.core.security.entity.DefaultUser;
import com.basksoft.core.security.entity.User;
import com.basksoft.core.security.provider.DefaultJakartaSecurityProvider;


/**
 * @author Sprite
 * @since 2022年12月6日
 */
public class MockSecurityProvider extends DefaultJakartaSecurityProvider {

	@Override
	public User getLoginUser(HttpServletRequest req) {
		User user=super.getLoginUser(req);
		if(user==null) {
			 ArrayList<Tenant> tenants = new ArrayList<Tenant>();
			 Tenant tenant=new Tenant();
			 tenant.setId("ddsoft");
			 tenant.setCreateUser("admin");
			 tenants.add(tenant);
			return new DefaultUser("admin", "admin User", true, tenants);
		}
		return user;
	}

	@Override
	public void login(HttpServletRequest req, String account, String password) {
		super.login(req, account, password);
	}

	@Override
	public void logout(HttpServletRequest req) {
		super.logout(req);
	}

}
