package com.basksoft.baskserver.demo.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.basksoft.baskserver.demo.ToolSpring;
import com.basksoft.core.BaskJakartaFilter;

@Configuration
public class BaskServerConfig implements WebMvcConfigurer,EnvironmentAware {
	
	@Bean
    public FilterRegistrationBean<BaskJakartaFilter> registerReportFilter() {
        FilterRegistrationBean<BaskJakartaFilter> registration = new FilterRegistrationBean<BaskJakartaFilter>(new BaskOriginFilter());
        registration.addUrlPatterns("/baskserver/*");
        registration.setName("bask");
        registration.setOrder(1);  
        return registration;
    }

	@Override
	public void setEnvironment(Environment environment) {
		ToolSpring.setEnvironment(environment);
	}
}
