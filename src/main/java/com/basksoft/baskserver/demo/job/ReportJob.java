package com.basksoft.baskserver.demo.job;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.basksoft.core.definition.Parameter;
import com.basksoft.core.definition.ParameterType;
import com.basksoft.core.job.BaskJob;
import com.basksoft.core.job.JobType;

public class ReportJob implements BaskJob {
	@Override
	public String run(String fileId, Map<String, Object> parameters) throws Exception {
		System.out.println(".. Report Job Run...");
		if (parameters.containsKey("test")) {
			throw new Exception("执行错误");
		}
		else {
			return "{}";
		}
	}

	@Override
	public JobType getType() {
		return JobType.report;
	}

	@Override
	public List<Parameter> getParameters(String fileId) {
		List<Parameter> paramters = new ArrayList<Parameter>();
		Parameter param = new Parameter();
		param.setDefaultValue("默认值");
		param.setDesc("用户邮箱");
		param.setName("email");
		param.setType(ParameterType.String);
		paramters.add(param);
		return paramters;
	}

}
