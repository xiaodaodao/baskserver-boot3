package com.basksoft.baskserver.demo.job;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.basksoft.core.definition.Parameter;
import com.basksoft.core.definition.ParameterType;
import com.basksoft.core.job.BaskJob;
import com.basksoft.core.job.JobType;

public class AdaptJob implements BaskJob {

	@Override
	public String run(String fileId, Map<String, Object> parameters) throws Exception {
		System.out.println(".. Adapt Job Run...");
		if (parameters.containsKey("test")) {
			throw new Exception("执行错误");
		}
		else {
			return "{}";
		}
	}

	@Override
	public JobType getType() {
		return JobType.adapt;
	}

	@Override
	public List<Parameter> getParameters(String fileId) {
		List<Parameter> paramters = new ArrayList<Parameter>();
		Parameter param = new Parameter();
		param.setDefaultValue("user@163.com");
		param.setDesc("用户邮箱");
		param.setName("email");
		param.setType(ParameterType.String);
		paramters.add(param);
		param = new Parameter();
		param.setDefaultValue("系统通知");
		param.setDesc("邮箱标题");
		param.setName("title");
		param.setType(ParameterType.String);
		paramters.add(param);
		return paramters;
	}

}
