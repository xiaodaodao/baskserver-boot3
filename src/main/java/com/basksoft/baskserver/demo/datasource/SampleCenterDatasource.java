package com.basksoft.baskserver.demo.datasource; 

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSourceFactory;
import org.springframework.core.env.Environment;

import com.basksoft.baskserver.demo.ToolSpring;
import com.basksoft.core.definition.datasource.DatasourceDefinition;
import com.basksoft.core.model.datasource.BuildinDatasourceProvider;

/**
 * @author William.Jiang
 * @since 2021年04月11日
 */

public class SampleCenterDatasource implements BuildinDatasourceProvider {
	private static Properties properties = new Properties();
	private DataSource dataSource = null;
	
	
	@Override
	public DataSource buildDatasource(DatasourceDefinition def) {
		if (dataSource!=null)  {
			return dataSource;
		}
		Environment env = ToolSpring.getBean(Environment.class);
		// 加载DBCP配置文件
		properties.put("username", System.getProperty("dbserver.user"));
		properties.put("password", System.getProperty("dbserver.pwd"));
		properties.put("driverClassName", env.getProperty("baskreport.sampledb.driverClassName"));
		properties.put("url",env.getProperty("baskreport.sampledb.url"));

		try {
			dataSource = BasicDataSourceFactory.createDataSource(properties);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dataSource;
		
	}
}
