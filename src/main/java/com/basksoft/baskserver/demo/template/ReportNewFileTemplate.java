package com.basksoft.baskserver.demo.template;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import com.basksoft.core.NewFileTemplate;
import com.basksoft.core.database.model.FileType;

public class ReportNewFileTemplate implements NewFileTemplate {

	@Override
	public String name() {
		return "报表模版";
	}

	@Override
	public String content() {
		String text = "";
		String fileName = "/Users/william/Products/product/basksoft/baskreport/baskreport-test/src/main/resources/test.xml";
		File file = new File(fileName);
		FileReader fr;
		try {
			fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null){
			    // 一行一行地处理...
			    System.out.println(line);
			    text+=line;
			}
			br.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		return text;
	}

	@Override
	public FileType type() {
		return FileType.report;
	}

	@Override
	public String tenantId() {
		return "basksoft";
	}

}
